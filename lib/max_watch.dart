abstract class MaxWatchUuids {
  static const String watchService =
      "6E400001-B5A3-F393-E0A9-E50E24DCCA9E";
  static const String watchTXCharacteristic =
      "6E400003-B5A3-F393-E0A9-E50E24DCCA9E";
  static const String watchRXCharacteristic =
      "6E400002-B5A3-F393-E0A9-E50E24DCCA9E";
}
