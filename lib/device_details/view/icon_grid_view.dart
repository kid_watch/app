
import 'package:flutter/material.dart';
import 'package:max_watch/device_details/device_details_bloc.dart';
import 'package:max_watch/device_details/view/icon_button.dart';
import 'package:max_watch/device_details/view/logs_container_view.dart';

class WatchGridView extends StatelessWidget {
  final DeviceDetailsBloc _deviceDetailsBloc;

  WatchGridView(this._deviceDetailsBloc);

  Widget build(BuildContext context) {

    _deviceDetailsBloc.connect();

    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(children: <Widget>[
        Expanded(
          flex: 8,
          child: SingleChildScrollView(
            child: _createControlPanel(),
          ),
        ),
        Expanded(
          flex: 1,
          child: LogsContainerView(_deviceDetailsBloc.logs),
        )
      ]),
    );
  }

  void _connect() {
    _deviceDetailsBloc.connect();
  }

  void _disconnect() {
    _deviceDetailsBloc.disconnectManual();
  }

  void _readRssi() {
    _deviceDetailsBloc.readRssi();
  }

  void _discovery() {
    _deviceDetailsBloc.discovery();
  }
  
  void _setPizza() {
    _deviceDetailsBloc.setIcon("PIZZA");
  }

  void _setIcon(String icon) {
    _deviceDetailsBloc.setIcon(icon);
  }

   void setTime() {
    _deviceDetailsBloc.setTime();
  }

  void _setPointUp() {
    _deviceDetailsBloc.setPointUp();
  }

  void _setPointDown() {
    _deviceDetailsBloc.setPointDown();
  }

  void _fetchConnectedDevices() {
    _deviceDetailsBloc.fetchConnectedDevices();
  }

  void _readCharacteristicForPeripheral() {
    _deviceDetailsBloc.readCharacteristicForPeripheral();
  }

  void _readCharacteristicForService() {
    _deviceDetailsBloc.readCharacteristicForService();
  }

  void _readCharacteristicDirectly() {
    _deviceDetailsBloc.readCharacteristicDirectly();
  }

  void _writeCharacteristicForPeripheral() {
    _deviceDetailsBloc.writeCharacteristicForPeripheral();
  }

  void _writeCharacteristicForService() {
    _deviceDetailsBloc.writeCharacteristicForService();
  }

  void _writeCharacteristicDirectly() {
    _deviceDetailsBloc.writeCharacteristicDirectly();
  }

  void _monitorCharacteristicForPeripheral() {
    _deviceDetailsBloc.monitorCharacteristicForPeripheral();
  }

  void _monitorCharacteristicForService() {
    _deviceDetailsBloc.monitorCharacteristicForService();
  }

  void _monitorCharacteristicDirectly() {
    _deviceDetailsBloc.monitorCharacteristicDirectly();
  }

  void _disableBluetooth() {
    _deviceDetailsBloc.disableBluetooth();
  }

  void _enableBluetooth() {
    _deviceDetailsBloc.enableBluetooth();
  }

  void _fetchBluetoothState() {
    _deviceDetailsBloc.fetchBluetoothState();
  }

  Column _createControlPanel() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 2.0),
          child: Row(
            children: <Widget>[
              IconButtonView("Connect", action: _connect),
              IconButtonView("Disconnect", action: _disconnect),
              IconButtonView("Sync time", action: setTime),
              //IconButtonView("Connected devices", action: _fetchConnectedDevices),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 2.0),
          child: Row(
            children: <Widget>[
              IconButtonView("point -", action: _setPointDown),
              IconButtonView("point +", action: _setPointUp),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 2.0),
          child: Row(
            children: <Widget>[
              IconButtonView("dress", action: _setIcon, icon: "SHIRT",),
              IconButtonView("bread", action: _setIcon, icon: "BREAD",),
              IconButtonView("run", action: _setIcon, icon: "RUNNING",),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 2.0),
          child: Row(
            children: <Widget>[
              IconButtonView("book", action: _setIcon, icon: "BOOK",),
              IconButtonView("puzzle", action: _setIcon, icon: "PUZZLE",),
              IconButtonView("guitar", action: _setIcon, icon: "GUITAR",),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 2.0),
          child: Row(
            children: <Widget>[
              IconButtonView("walk", action: _setIcon, icon: "WALK",),
              IconButtonView("tree", action: _setIcon, icon: "TREE",),
              IconButtonView("scooter", action: _setIcon, icon: "SCOOTER",),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 2.0),
          child: Row(
            children: <Widget>[
              IconButtonView("pizza", action: _setIcon, icon: "PIZZA",),
              IconButtonView("carrot", action: _setIcon, icon: "CARROT",),
              IconButtonView("bacon", action: _setIcon, icon: "BACON",),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 2.0),
          child: Row(
            children: <Widget>[
              IconButtonView("wash hands", action: _setIcon, icon: "HWASH",),
              IconButtonView("TV", action: _setIcon, icon: "TV",),
              IconButtonView("music", action: _setIcon, icon: "music",),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 2.0),
          child: Row(
            children: <Widget>[
              IconButtonView("shower", action: _setIcon, icon: "SHOWER",),
              IconButtonView("brush teeth", action: _setIcon, icon: "TBRUSH",),
              IconButtonView("sleep", action: _setIcon, icon: "BED",),
            ],
          ),
        ),
      ],
    );
  }
}
