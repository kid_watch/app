
import 'package:flutter/material.dart';

class IconButtonView extends StatelessWidget {

  final String _text;
  final Function action;
  final String icon;

  IconButtonView(this._text, { this.action, this.icon });

  @override
  Widget build(BuildContext context) {

    var doAction = action;

    if(icon != null) {
      doAction = () { Function.apply(action, [icon.toUpperCase()]); };
    }

    var image = Image.asset('assets/icons/bed.png', height: 5);
    if(icon != null) {
      image = Image.asset('assets/icons/'+icon.toLowerCase()+'.png', height: 50);
    }

    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(2.0),
        child: RaisedButton(
          color: Colors.blue,
          textColor: Colors.white,
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              image,
              Text(_text),
            ] ),
          //onPressed: Function.apply(action, [icon]),
          onPressed: doAction,
        ),
      ),
    );
  }
}